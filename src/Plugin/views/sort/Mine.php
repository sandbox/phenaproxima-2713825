<?php

/**
 * @file
 * Contains \Drupal\views_mine\Plugin\views\sort\Mine.
 */

namespace Drupal\views_mine\Plugin\views\sort;

use Drupal\Core\Session\AccountInterface;
use Drupal\views\Plugin\views\query\Sql;
use Drupal\views\Plugin\views\sort\SortPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @ViewsSort("mine")
 */
class Mine extends SortPluginBase {

  /**
   * The currently logged-in user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    if ($this->query instanceof Sql) {
      $this->ensureMyTable();
      $expression = sprintf('IF (%s.%s = %d, 1, 0)', $this->tableAlias, $this->definition['entity field'], $this->currentUser->id());
      $this->query->addOrderBy(NULL, $expression, $this->options['order'], $this->getPluginId());
    }
  }

}
