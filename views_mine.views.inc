<?php

/**
 * Implements hook_views_data_alter().
 */
function views_mine_views_data_alter(array &$data) {
  foreach (\Drupal::entityTypeManager()->getDefinitions() as $entity_type) {
    $table = $entity_type->getDataTable() ?: $entity_type->getBaseTable();

    if (isset($data[$table]) && $entity_type->hasKey('uid')) {
      $data[$table]['mine'] = [
        'title' => t('Owned by current user'),
        'help' => t('Whether the entity is owned by the currently logged-in user.'),
        'sort' => [
          'id' => 'mine',
        ],
        'entity field' => $entity_type->getKey('uid'),
      ];
    }
  }
}
